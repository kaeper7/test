@extends('layouts.app')

@section('content')

<div class="col-lg-7 col-md-9">
<br>


@if ($book)
    <h1>Editar Libro</h1>
    {!! Form::open(['route' =>  ['book.update', $book->id], 'method' => 'PUT']) !!}
@else
    <h1>Crear Libro</h1>
    {!! Form::open(['route' => 'book.store', 'method' => 'POST']) !!}
@endif

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {{ Form::label('name', 'Nombre') }}
            {{ Form::text('name', $book ? $book->name : ''   , ['class' => 'form-control']) }}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('author') ? ' has-error' : '' }}">
            {{ Form::label('author', 'Autor') }}
            {{ Form::text('author', $book ? $book->author : ''   , ['class' => 'form-control']) }}
            @if ($errors->has('author'))
                <span class="help-block">
                    <strong>{{ $errors->first('author') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
            {{ Form::label('category_id', 'Categoría') }}
            {{ Form::text('category_id', null, array('placeholder' => 'Ingresa el nombre de una categoría','class' => 'form-control','id'=>'category_id')) }}
            @if ($errors->has('category_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('category_id') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('published_at') ? ' has-error' : '' }}">
            {{ Form::label('published_at', 'Publicado') }}
            {{ Form::date('published_at', $book ? $book->published_at : \Carbon\Carbon::now()   , ['class' => 'form-control']) }}
            @if ($errors->has('published_at'))
                <span class="help-block">
                    <strong>{{ $errors->first('published_at') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('user') ? ' has-error' : '' }}">
            {{ Form::label('user', 'Usuario') }}
            {{ Form::text('user', $book ? $book->user : ''   , ['class' => 'form-control']) }}
            @if ($errors->has('user'))
                <span class="help-block">
                    <strong>{{ $errors->first('user') }}</strong>
                </span>
            @endif
        </div>

    {{ link_to_route('book.index', 'Cancelar', null, ['class'=>'btn btn-danger']) }}
    {{ Form::submit( $book ? 'Editar' : 'Crear',['class'=>'btn btn-primary']) }}

{!! Form::close() !!}

</div>
@endsection


<script>
$(document).ready(function() {
 src = "{{ route('autocomplete') }}";
  $("#category_id").autocomplete({
     source: function(request, response) {
         $.ajax({
             url: src,
             dataType: "json",
             data: {
                 term : request.term
             },
             success: function(data) {
                 response(data);

             }
         });
     },
     minLength: 3,

 });
});
</script>
