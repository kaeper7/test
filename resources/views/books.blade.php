@extends('layouts.app')

@section('content')

<div class="col-lg-8 col-md-9">
    <div>
    	<h1 style="float: left;">Libros</h1>
    	<a href="{{ route('book.create') }} " class="btn btn-primary" style="width: 20%; float: right;">Agregar libro</a>
    </div>

    <table class="table table-hover">
    	<thead>
    		<tr>
    			<th> Nombre </th>
    			<th> Autor </th>
    			<th> Categoría </th>
          <th> Publicación </th>
          <th> Usuario </th>
          <th> Acciones </th>
    		</tr>
    	</thead>
    	<tbody>
    		@foreach($books as $book)
    		<tr data-href="{{ route('book.edit', $book->id) }}">
    			<td class="clickable-row">{{ $book->name }}</td>
    			<td class="clickable-row" >{{ $book->author }}</td>
          <td class="clickable-row" >{{ $book->Category->name }}</td>
          <td class="clickable-row" >{{ $book->published_at }}</td>
          <td class="clickable-row" >{{ $book->user }}</td>
    			<td>
    				<a href="{{ route('book.edit', $book->id) }}"> <button type="button" class="btn btn-primary">Editar</button> </a>
    				<a href="{{ route('book.destroy', $book->id) }}" class="btn btn-danger delete-element"> Eliminar </a>
    			</td>
    		</tr>
    		@endforeach
    	</tbody>
    </table>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('js/delete_element.js') }}"></script>
@endpush
