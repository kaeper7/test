@extends('layouts.app')

@section('content')

<div class="col-lg-8 col-md-9">
    <div>
    	<h1 style="float: left;">Categorías</h1>
    	<a href="{{ route('category.create') }} " class="btn btn-primary" style="width: 20%; float: right;">Crear categoría</a>
    </div>

    <table class="table table-hover">
      	<thead>
        		<tr>
          			<th> Nombre </th>
          			<th> Descripción </th>
          			<th> Libros </th>
                <th> Acciones </th>
        		</tr>
      	</thead>

      	<tbody>
        		@foreach($categories as $category)
        		<tr data-href="{{ route('category.edit', $category->id) }}">
          			<td class="clickable-row">{{ $category->name }}</td>
          			<td class="clickable-row" >{{ $category->description }}</td>
                <td></td>
          			<td>
          				<a href="{{ route('category.edit', $category->id) }}"> <button type="button" class="btn btn-primary">Editar</button> </a>
          				<a href="{{ route('category.destroy', $category->id) }}" class="btn btn-danger delete-element"> Eliminar </a>
          			</td>
        		</tr>
        		@endforeach
      	</tbody>

    </table>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('js/delete_element.js') }}"></script>
@endpush
