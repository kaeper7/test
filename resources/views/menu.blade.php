<div class="col-md-3" style="min-height: 100%;">
  <div class="wrapper" style="display: flex; width: 100%; align-items: stretch;">
    <nav id="sidebar">
          <div class="sidebar-header">
              <h3>Library Sidebar</h3>
          </div>

          <ul class="list-unstyled components">
              <p>Menu</p>
              <li class="active">
                  <a href="{{ route('book.index') }} " aria-expanded="false" class="dropdown-toggle">Libros</a>
              </li>
              <li class="active">
                  <a href="{{ route('category.index') }} " aria-expanded="false" class="dropdown-toggle">Categorías</a>
              </li>
          </ul>
      </nav>

  </div>

</div>

<!-- https://bootstrapious.com/p/bootstrap-sidebar -->
