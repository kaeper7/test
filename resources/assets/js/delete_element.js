$(function() {
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

	$('.delete-element').click(function (e) {
		var response = confirm("¿Desea eleminar este elemento?");
		e.preventDefault();

		if(response){
			console.log('Elimina elemento');
			$.ajax({
			    url: $(this).attr('href'),
			    type: 'delete'
			}).done(function (response) {
				location.reload();
			});
		}
	})

});
