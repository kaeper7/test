<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Category::all();
        return view('categories')->with('categories', $records);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('category_create')->with('category', '');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'name' => 'required',
          'description' => 'required',
      ]);

      $category = new Category($request->all());
      $category->save();

      return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $records = Category::find($id);
      return view('category_create')->with('category', $records);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
          'name' => 'required',
          'description' => 'required',
      ]);

      $category = Category::find($id);

      // Update company data
      $categoryUpdate = $request->all();
      $category->update($categoryUpdate);

      return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $category = Category::find($id);
      $category->delete();

      return response()->json(['status' => true]);
    }

    public function autocomplete(Request $request){
        $term = Input::get('term');

        $results = array();

        $queries = DB::table('categories')
          ->where('name', 'LIKE', '%'.$term.'%')
          ->take(5)->get();

        foreach ($queries as $query) {
            $results[] = [ 'id' => $query->id, 'value' => $query->name ];
        }

        return Response::json($results);
    }
}
