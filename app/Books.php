<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    public $fillable = [
        'id',
        'name',
        'author',
        'category_id',
        'published_at',
        'user'
    ];

    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

}
