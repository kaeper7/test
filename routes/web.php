<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::group(['middleware' => 'auth'], function(){
    // Controllers Within The "App\Http\Controllers\" Namespace
	//Route::get('/categories', 'CategoryController@index')->name('category');
	Route::resource('category', 'CategoryController');
  Route::resource('book', 'BookController');
  Route::get("autocomplete",array('as'=>'autocomplete','category_id'=> 'CategoryController@autocomplete'));
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
